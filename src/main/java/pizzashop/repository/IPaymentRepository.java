package pizzashop.repository;

import pizzashop.model.Payment;

import java.util.List;

public interface IPaymentRepository {
    Payment add(Payment payment);
    List<Payment> getAll();
    void writeAll();

}
