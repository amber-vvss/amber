package pizzashop.repository;

import pizzashop.model.Payment;

import java.util.ArrayList;
import java.util.List;

public class PaymentRepositoryInMemory implements IPaymentRepository {

    private List<Payment> payments;

    public PaymentRepositoryInMemory() {
        this.payments = new ArrayList<>();
    }

    @Override
    public Payment add(Payment payment) {
        payments.add(payment);
        return payment;
    }

    @Override
    public List<Payment> getAll() {
        return payments;
    }

    @Override
    public void writeAll() {
        // do nothing
    }
}
