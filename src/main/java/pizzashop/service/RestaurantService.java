package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.IMenuRepository;
import pizzashop.repository.IPaymentRepository;

import java.util.List;

public class RestaurantService implements IRestaurantService {

    private IMenuRepository menuRepo;
    private IPaymentRepository payRepo;

    public RestaurantService(IMenuRepository menuRepo, IPaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
    }

    public List<MenuDataModel> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    public Payment addPayment(int table, PaymentType type, double amount) throws Exception {
        String error = "";

        if (amount <= 0) {
            error += "Please choose at least one item!\n";
        }

        if (!(table >= 1 && table <= 8)) {
            error += "Invalid table number!\n";
        }

        if (!error.isEmpty()) {
            throw new Exception(error);
        }

        Payment payment = new Payment(table, type, amount);
        payRepo.add(payment);
        return payment;
    }

    public double getTotalAmount(PaymentType type) {
        double total = 0.0f;
        List<Payment> payments = getPayments();

        if (type == null) {
            return total;
        }

        if (payments.isEmpty()) {
            return total;
        }

        for (Payment p : payments) {
            if (p.getType().equals(type)) {
                total += p.getAmount();
            }
        }
        return total;
    }

}