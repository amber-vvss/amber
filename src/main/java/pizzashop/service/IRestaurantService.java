package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.util.List;

public interface IRestaurantService {
    List<MenuDataModel> getMenuData();
    List<Payment> getPayments();
    Payment addPayment(int table, PaymentType type, double amount) throws Exception;
    double getTotalAmount(PaymentType type);
}
