package pizzashop.service;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import pizzashop.model.PaymentType;

import java.util.Optional;

public class PaymentAlert implements PaymentOperation {
    private IRestaurantService service;

    public PaymentAlert(IRestaurantService service) {
        this.service = service;
    }

    @Override
    public void cardPayment() {
        System.out.println("--------------------------");
        System.out.println("Paying by card...");
        System.out.println("Please insert your card!");
        System.out.println("--------------------------");
    }

    @Override
    public void cashPayment() {
        System.out.println("--------------------------");
        System.out.println("Paying cash...");
        System.out.println("Please show the cash...!");
        System.out.println("--------------------------");
    }

    @Override
    public void cancelPayment() {
        System.out.println("--------------------------");
        System.out.println("Payment choice needed...");
        System.out.println("--------------------------");
    }

    public void showPaymentAlert(int tableNumber, double totalAmount) throws Exception {
        if (totalAmount <= 0) {
            throw new Exception("Please choose at least one item!");
        }
        Alert paymentAlert = new Alert(Alert.AlertType.CONFIRMATION);
        paymentAlert.setTitle("Payment for Table " + tableNumber);
        paymentAlert.setHeaderText("Total amount: " + totalAmount);
        paymentAlert.setContentText("Please choose payment option");
        ButtonType cardPayment = new ButtonType("Pay by Card");
        ButtonType cashPayment = new ButtonType("Pay Cash");
        ButtonType cancel = new ButtonType("Cancel");
        paymentAlert.getButtonTypes().setAll(cardPayment, cashPayment, cancel);
        Optional<ButtonType> result = paymentAlert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == cardPayment) {
                cardPayment();
                service.addPayment(tableNumber, PaymentType.CARD, totalAmount);
            } else if (result.get() == cashPayment) {
                cashPayment();
                service.addPayment(tableNumber, PaymentType.CASH, totalAmount);
            } else {
                cancelPayment();
            }
        }
    }

    public void showInvalidAlert(String message) {
        Alert paymentAlert = new Alert(Alert.AlertType.WARNING);
        paymentAlert.setTitle("Warning!");
        paymentAlert.setHeaderText(message);
        ButtonType okButton = new ButtonType("OK");
        paymentAlert.getButtonTypes().setAll(okButton);
        paymentAlert.showAndWait();
    }
}