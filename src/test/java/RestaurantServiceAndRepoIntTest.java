import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.IMenuRepository;
import pizzashop.repository.IPaymentRepository;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepositoryInMemory;
import pizzashop.service.RestaurantService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

public class RestaurantServiceAndRepoIntTest {
    private IPaymentRepository paymentRepository;
    private IMenuRepository menuRepository;
    private RestaurantService restaurantService;
    private Payment payment;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        paymentRepository = new PaymentRepositoryInMemory();
        menuRepository = new MenuRepository();
        restaurantService = new RestaurantService(menuRepository, paymentRepository);
        payment = mock(Payment.class);
    }

    @Test
    public void testGetPayments() {
        paymentRepository.add(payment);
        List<Payment> paymentsFromService = restaurantService.getPayments();
        assertEquals(Arrays.asList(payment), paymentsFromService);
    }

    @Test
    public void testGetPayments_empty() {
        List<Payment> paymentsFromService = restaurantService.getPayments();
        assertEquals(Collections.emptyList(), paymentsFromService);
    }
}