import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.IMenuRepository;
import pizzashop.repository.IPaymentRepository;
import pizzashop.service.RestaurantService;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public class RestaurantServiceUnitTest {
    @Mock
    private IPaymentRepository paymentRepository;

    @Mock
    private IMenuRepository menuRepository;

    @InjectMocks
    private RestaurantService restaurantService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetPayments() {
        Payment payment = new Payment(1, PaymentType.CASH, 10.0d);
        List<Payment> payments = Arrays.asList(payment);
        Mockito.when(paymentRepository.getAll()).thenReturn(payments);
        List<Payment> result = restaurantService.getPayments();
        Mockito.verify(paymentRepository, times(1)).getAll();
        Mockito.verify(paymentRepository, never()).add(payment);
        assert result.size() == 1;
        assert result.get(0).getAmount() == 10.0;
        assert result.get(0).getType() == PaymentType.CASH;
    }

    @Test
    public void testGetPaymentsEmpty() {
        List<Payment> payments = Collections.emptyList();
        Mockito.when(paymentRepository.getAll()).thenReturn(payments);
        List<Payment> result = restaurantService.getPayments();
        Mockito.verify(paymentRepository, times(1)).getAll();
        Mockito.verify(paymentRepository, never()).add(Mockito.any(Payment.class));
        assert result.size() == 0;
    }

    @Test
    public void testGetPaymentsNull() {
        List<Payment> payments = null;
        Mockito.when(paymentRepository.getAll()).thenReturn(payments);
        List<Payment> result = restaurantService.getPayments();
        Mockito.verify(paymentRepository, times(1)).getAll();
        Mockito.verify(paymentRepository, never()).add(Mockito.any(Payment.class));
        assert result == null;
    }

    @Test
    public void testAddPayment() {
        Payment payment = new Payment(1, PaymentType.CASH, 10.0d);
        Mockito.when(paymentRepository.add(payment)).thenReturn(payment);
        Payment result = null;
        try {
            result = restaurantService.addPayment(1, PaymentType.CASH, 10.0d);
        } catch (Exception e) {
            fail();
        }
        Mockito.verify(paymentRepository, times(1)).add(payment);
        Mockito.verify(paymentRepository, never()).getAll();
        assert result.getAmount() == 10.0;
        assert result.getType() == PaymentType.CASH;
    }

    @Test
    public void testAddPaymentEmpty() {
        Payment payment = new Payment(1, PaymentType.CASH, 10.0d);
        Mockito.when(paymentRepository.add(payment)).thenReturn(payment);
        Payment result = null;
        try {
            result = restaurantService.addPayment(1, PaymentType.CASH, 10.0d);
        } catch (Exception e) {
            fail();
        }

        Mockito.verify(paymentRepository, times(1)).add(payment);
        Mockito.verify(paymentRepository, never()).getAll();
        assert result.getAmount() == 10.0;
        assert result.getType() == PaymentType.CASH;
    }
}