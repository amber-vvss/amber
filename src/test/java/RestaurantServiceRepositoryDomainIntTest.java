import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.IMenuRepository;
import pizzashop.repository.IPaymentRepository;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepositoryInMemory;
import pizzashop.service.RestaurantService;

import static org.junit.jupiter.api.Assertions.fail;

public class RestaurantServiceRepositoryDomainIntTest {
    private Payment payment;
    private IPaymentRepository paymentRepository;
    private IMenuRepository menuRepository;
    private RestaurantService restaurantService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        payment = new Payment(1, PaymentType.CASH, 100);
        paymentRepository = new PaymentRepositoryInMemory();
        menuRepository = new MenuRepository();
        restaurantService = new RestaurantService(menuRepository, paymentRepository);
    }

    @Test
    public void testPaymentWithInvalidTableNumber() {
        try {
            restaurantService.addPayment(0, payment.getType(), payment.getAmount());
            fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof Exception);
            Assertions.assertTrue(e.getMessage().equals("Invalid table number!\n"));
        }
    }

    @Test
    public void testPaymentWithInvalidAmount() {
        try {
            restaurantService.addPayment(payment.getTableNumber(), payment.getType(), -1);
            fail();
        } catch (Exception e) {
            Assertions.assertTrue(e instanceof Exception);
            Assertions.assertTrue(e.getMessage().equals("Please choose at least one item!\n"));
        }
    }
}
