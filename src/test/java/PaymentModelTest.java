import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentModelTest {

    private Payment payment;
    private double amount = 20.5d;
    private PaymentType type = PaymentType.CASH;
    private Integer tableNumber = 1;

    @BeforeAll
    void setup() {
        payment = new Payment(tableNumber, type, amount);
    }

    @Test
    void testGetAmount() {
        assertEquals(amount, payment.getAmount());
    }

    @Test
    void testGetType() {
        assertEquals(type, payment.getType());
    }

    @Test
    void testGetTableNumber() {
        assertEquals(tableNumber, payment.getTableNumber());
    }
}
