import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.repository.PaymentRepositoryInMemory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class PaymentRepositoryTest {

    private PaymentRepositoryInMemory repo;

    private Payment payment;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repo = new PaymentRepositoryInMemory();
        payment = mock(Payment.class);
    }

    @Test
    public void testAdd() {
        repo.add(payment);
        assertEquals(1, repo.getAll().size());
        assertEquals(payment, repo.getAll().get(0));
    }

    @Test
    public void testGet() {
        repo.add(payment);
        Payment payment1 = repo.getAll().get(0);
        assertEquals(payment, payment1);
    }

    @AfterEach
    public void tearDown() {
        repo = null;
        payment = null;
    }
}
