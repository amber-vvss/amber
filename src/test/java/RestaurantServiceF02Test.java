import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.RestaurantService;

import static org.junit.jupiter.api.Assertions.*;

public class RestaurantServiceF02Test {
    private MenuRepository repoMenu;
    private PaymentRepository payRepo;
    private RestaurantService service;

    @BeforeEach
    void setUp() {
        this.repoMenu = new MenuRepository();
    }

    @Test
    void test_print_valid_total_amount() {
        // Arrange
        this.payRepo = new PaymentRepository("paymentsAmount.txt");
        this.service = new RestaurantService(repoMenu, payRepo);
        final double expectedTotalAmount = 243.25d;
        double amount;

        // Act
        amount = service.getTotalAmount(PaymentType.CARD);

        // Assert
        assertEquals(expectedTotalAmount, amount);
    }

    @Test
    void test_print_nonvalid_total_amount() {
        // Arrange
        this.payRepo = new PaymentRepository("paymentsNonValid.txt");
        this.service = new RestaurantService(repoMenu, payRepo);
        final double expectedTotalAmount = 0d;
        double amount;

        // Act
        amount = service.getTotalAmount(PaymentType.CARD);

        // Assert
        assertEquals(expectedTotalAmount, amount);
    }

    @Test
    void test_print_total_amount() {
        // Arrange
        this.payRepo = new PaymentRepository("paymentsAmount.txt");
        this.service = new RestaurantService(repoMenu, payRepo);
        final double expectedTotalAmount = 0d;
        double amount;

        // Act
        amount = service.getTotalAmount(null);

        // Assert
        assertEquals(expectedTotalAmount, amount);
    }
}
