import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.RestaurantService;

import static org.junit.jupiter.api.Assertions.*;

class RestaurantServiceTest {
    private MenuRepository repoMenu;
    private PaymentRepository payRepo;
    private RestaurantService service;

    @BeforeEach
    void setUp() {
        this.repoMenu = new MenuRepository();
        this.payRepo = new PaymentRepository("payments.txt");
        this.service = new RestaurantService(repoMenu, payRepo);
    }

    // TC1_ECP
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6, 7, 8})
    @Tag("ECP_valid_table")
    void Test_addPayment_ECP_valid_table(int table) {
        // Arrange
        Payment payment = new Payment(table, PaymentType.CASH, 5.2d);

        // Act
        try {
            service.addPayment(table, PaymentType.CASH, 5.2d);
        } catch (Exception e) {
            fail();
        }

        // Assert
        assertEquals(payment, service.getPayments().get(service.getPayments().size() - 1));
    }

    // TC2_ECP
    @ParameterizedTest
    @ValueSource(ints = {0, 10})
    @DisplayName("nonValid table number must fail.")
    void Test_addPayment_ECP_nonValid_table(int table) {
        // Arrange
        Payment payment = new Payment(table, PaymentType.CASH, 8.4d);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Invalid table number!\n", exception.getMessage());
    }

    @Disabled
    @ParameterizedTest
    @ValueSource(doubles = {1.2, 2.45, 3.99, 4.55, 5.01, 6.02, 75.0})
    void Test_addPayment_ECP_valid_amount(double amount) {
        // Arrange
        Payment payment = new Payment(1, PaymentType.CASH, amount);

        // Act
        try {
            service.addPayment(1, PaymentType.CASH, amount);
        } catch (Exception e) {
            fail();
        }

        // Assert
        assertEquals(payment, service.getPayments().get(service.getPayments().size() - 1));
    }

    // TC3_ECP
    @ParameterizedTest
    @ValueSource(doubles = {0.0, -1.2, -5.25})
    @DisplayName("nonValid amount value must fail.")
    void Test_addPayment_ECP_nonValid_amount(double amount) {
        // Arrange
        Payment payment = new Payment(5, PaymentType.CASH, amount);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Please choose at least one item!\n", exception.getMessage());
    }

    // TC4_ECP
    @RepeatedTest(10)
    void Test_addPayment_ECP_nonValid_table_amount() {
        // Arrange
        Payment payment = new Payment(15, PaymentType.CARD, 0);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Please choose at least one item!\nInvalid table number!\n", exception.getMessage());
    }

    // TC1_BVA
    @Test
    void Test_addPayment_BVA_nonValid_amount_left() {
        // Arrange
        Payment payment = new Payment(1, PaymentType.CARD, -0.01);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Please choose at least one item!\n", exception.getMessage());
    }

    // TC2_BVA
    @Test
    void Test_addPayment_BVA_nonValid_amount_middle() {
        // Arrange
        Payment payment = new Payment(2, PaymentType.CARD, 0);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Please choose at least one item!\n", exception.getMessage());
    }

    // TC3_BVA
    @Test
    void Test_addPayment_BVA_valid_amount_right() {
        // Arrange
        Payment payment = new Payment(3, PaymentType.CASH, 0.01d);

        // Act
        try {
            service.addPayment(3, PaymentType.CASH, 0.01d);
        } catch (Exception e) {
            fail();
        }

        // Assert
        assertEquals(payment, service.getPayments().get(service.getPayments().size() - 1));
    }

    // TC8_BVA
    @Test
    void Test_addPayment_BVA_valid_table_max() {
        // Arrange
        Payment payment = new Payment(8, PaymentType.CARD, 8.24d);

        // Assert
        try {
            service.addPayment(8, PaymentType.CARD, 8.24d);
        } catch (Exception e) {
            fail();
        }
        assertEquals(payment, service.getPayments().get(service.getPayments().size() - 1));
    }

    // TC9_BVA
    @Test
    void Test_addPayment_BVA_nonValid_table_min() {
        // Arrange
        Payment payment = new Payment(0, PaymentType.CARD, 1.2d);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Invalid table number!\n", exception.getMessage());
    }

    // TC10_BVA
    @Test
    void Test_addPayment_BVA_nonValid_table_max() {
        // Arrange
        Payment payment = new Payment(9, PaymentType.CARD, 1.2d);

        // Assert
        Exception exception = assertThrows(Exception.class, () -> service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount()));
        assertEquals("Invalid table number!\n", exception.getMessage());
    }
}